package com.example.servicebai18;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class DestinationActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    @Override
    //Class này là khi người dùng ấn vào thông báo ở trên màn hình
    //sẽ là màn hình thời tiết
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        mediaPlayer.stop();
    }
}