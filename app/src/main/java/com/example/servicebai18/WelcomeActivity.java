package com.example.servicebai18;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.auth.FirebaseAuth;

public class WelcomeActivity extends AppCompatActivity {
    ProgressBar progressBar;
    FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);



    }
    public void login(View view){
        startActivity(new Intent(WelcomeActivity.this , LoginActivity.class));

    }
    public void registration(View view){
        startActivity(new Intent(WelcomeActivity.this , RegistrationActivity.class));

    }
}