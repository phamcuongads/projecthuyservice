package com.example.servicebai18.ui.home;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.servicebai18.AlarmAdapter;
import com.example.servicebai18.AlarmReceiver;
import com.example.servicebai18.MyService;
import com.example.servicebai18.R;
import com.example.servicebai18.database.Database;
import com.example.servicebai18.databinding.FragmentHomeBinding;
import com.example.servicebai18.model.AlarmModel;
import com.example.servicebai18.model.OnitemClick;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HomeFragment extends Fragment implements OnitemClick {

    TextView txtTime , tvClock;
    private Button btnSelectTime;
    private Button btnCancel;
    private Button btnSetTime;
    private Calendar calendar;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    RecyclerView rcvAlarm;
    private List<AlarmModel> list;
    private AlarmAdapter adapter;
    private MaterialTimePicker picker;
    private String time;

    Database dao;

    private FragmentHomeBinding binding;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        dao = new Database(root.getContext(), null, null, 1, null);
        //Khởi tạo các đối tượng trên view
        btnCancel = root.findViewById(R.id.btnCancel);
        txtTime = root.findViewById(R.id.selectTime);
        btnSelectTime = root.findViewById(R.id.btnSelectTime);
        btnSetTime = root.findViewById(R.id.btnSetAlarm);
        rcvAlarm = root.findViewById(R.id.rcvArlarm);
        tvClock = root.findViewById(R.id.tv_timeClock);

        tvClock.setText("");

        //Get dữ liệu từ DB
        list = new ArrayList<>();
        list = dao.selectAll();
        //Duyệt vòng for để tất cả các giờ báo thức có trong DB
        for (int i = 0; i < list.size(); i++) {
            AlarmModel a = list.get(i);
            String time = a.getTime();
            String[] timeSlip = time.split(":");
            //Khỏi tạo 1 lớp Calendar
            calendar = Calendar.getInstance();
            //SET Giờ cho calendar
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeSlip[0].trim()));
            //SET Phút cho calendar
            calendar.set(Calendar.MINUTE, Integer.parseInt(timeSlip[1].trim()));
//            Khởi tạo thư viện Alarm
            alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getContext(), AlarmReceiver.class);
            //Set thông báo cho pending intent
            pendingIntent = PendingIntent.getBroadcast(root.getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//            AlarmManager alarmManager = (AlarmManager) getView().getContext().getSystemService(Context.ALARM_SERVICE);
            //Set thời gian báo thức
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);
        }
        //Gọi đến Adapter để set list cho recycleView
        adapter = new AlarmAdapter(list, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rcvAlarm.setLayoutManager(linearLayoutManager);
        rcvAlarm.setAdapter(adapter);
        //Hiển thị giờ hiện tại
        HourCurrent hourCurrent = new HourCurrent();
        hourCurrent.start();

//        try {
//            bindingView();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        bindingAction();
        createNotificationChange();



//        final TextView textView = binding.textHome;
//        homeViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    // Event onClick
    private void bindingAction() {
        btnSelectTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowTimePicker();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelAlarm();
            }
        });
        btnSetTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAlarm();
            }
        });
    }

    // Notification
    private void createNotificationChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "báo thức";
            String description = "Chanel for alrm ";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("huynv", name, importance);
            notificationChannel.setDescription(description);
            NotificationManager notificationManager = getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);

        }
    }
    // show Clock pick time
    private void ShowTimePicker() {
        //Show cái TimePicker
        //Có thể lấy được giwof hiện tại để đưa vào chỗ chọn thời gian
        //Đang để mặc định 12h AM
        picker = new MaterialTimePicker.Builder().setTimeFormat(TimeFormat.CLOCK_12H)
                .setHour(12).setMinute(0)
                .setTitleText("Select Alarm Time").build();
        //Show
        picker.show(getFragmentManager(), "huynv");
        //Khi click vào nút ok trên time picker
        picker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (picker.getMinute() < 10) {
                    txtTime.setText(picker.getHour() + " : 0" + picker.getMinute());
                }
                if (picker.getHour() < 10) {
                    txtTime.setText("0" + picker.getHour() + " : " + picker.getMinute());
                }
                if (picker.getHour() < 10 && picker.getMinute() < 10) {
                    txtTime.setText("0" + picker.getHour() + " : 0" + picker.getMinute());
                } else {
                    txtTime.setText(picker.getHour() + " : " + picker.getMinute());
                }
                //Set thời gian vào calender để đưa vào Alarm báo thức
                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, picker.getHour());
                calendar.set(Calendar.MINUTE, picker.getMinute());
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
            }
        });

    }

    private void setAlarm() {

        AlarmModel alarmModel = new AlarmModel();
        alarmModel.setTime(txtTime.getText().toString());
        alarmModel.setDescription(txtTime.getText().toString());
        alarmModel.setValue(true);


        dao.insertDB(alarmModel);
        list = dao.selectAll();
        //Update lại list hiển thị list trên recycleView
        //Nhưng đang lỗi
        adapter.updateData(list);
//        set thời gian được thêm vào alarm để cài báo thức
        alarmManager = (AlarmManager) getView().getContext().getSystemService(Context.ALARM_SERVICE);
//        Gọi đến cái thông báo
        Intent intent = new Intent(getContext(), AlarmReceiver.class);
//        đưa cái thông báo vào Pending iten
        pendingIntent = PendingIntent.getBroadcast(getView().getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        khởi tạo Alarm Manage
        alarmManager = (AlarmManager) getView().getContext().getSystemService(Context.ALARM_SERVICE);
//        set thời gian cho Alarm. Cài đặt h báo thức
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
        Toast.makeText(getContext(), "Alarm set Successfully", Toast.LENGTH_SHORT).show();
        // set service for Alarm

        // bat dau sv
//        Intent intent1 = new Intent(getContext() , MyService.class);
//        getActivity().startService(intent1);
//        Toast.makeText(getContext(), "start sv success", Toast.LENGTH_SHORT).show();

    }


    // Cancel Alarm
    private void cancelAlarm() {
        // sẽ gọi đến cái thông báo khi đến giờ báo thức
//        Intent intent = new Intent(this, AlarmReceiver.class);
        Intent intent = new Intent(getContext(), AlarmReceiver.class);
//        đưa vào 1 cái pendingintent
        pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent, 0);
        if (alarmManager == null) {
            alarmManager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
        }
        //Xóa báo thức
        alarmManager.cancel(pendingIntent);
        Toast.makeText(getContext(), "Cancel Successfully", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onItemClick(String id, boolean check) {
        if (check == true) {
            dao.UpdateClock(Integer.parseInt(id), 0);
        } else {
            dao.UpdateClock(Integer.parseInt(id), 1);
        }
        Toast.makeText(getContext(), "On click" + id, Toast.LENGTH_SHORT).show();
        list = dao.selectAll();
        adapter = new AlarmAdapter(list, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        rcvAlarm.setLayoutManager(linearLayoutManager);
        rcvAlarm.setAdapter(adapter);
    }

    // Hien thi gio 1 cach lien tuc
@RequiresApi(api = Build.VERSION_CODES.O)
class HourCurrent extends Thread {
    public void run() {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();
            txtTime.setText(dtf.format(now));
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}







    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}