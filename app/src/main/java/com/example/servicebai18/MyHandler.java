package com.example.servicebai18;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.Provider;
import java.util.Calendar;

public class MyHandler extends Handler{

    private static final String CHANNEL_DEFAULT_IMPORTANCE = "";
    private Service service;
    Calendar calendar;

    public MyHandler(@NonNull Looper looper , Service service) {
        super(looper);
        this.service = service;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void handleMessage(@NonNull Message msg) {
        super.handleMessage(msg);


//        Intent myIntent = new Intent(service.getApplicationContext(),NavigationActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(service.getApplicationContext(),0,myIntent,0);


        NotificationManager manager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("myChanel","myChanel",NotificationManager.IMPORTANCE_HIGH);
        manager.createNotificationChannel(channel);


        Notification.Builder builder = new Notification.Builder(service.getApplicationContext(),"myChanel");

        builder.setContentTitle("Thong Bao");
        builder.setContentText("Den gio roi");
        builder.setSmallIcon(R.drawable.iconminus);
//        builder.setContentIntent(pendingIntent);
        Notification notification = builder.build();
        manager.notify(100,notification);


// Notification ID cannot be 0.
        service.startForeground(123,notification);

//              service.stopSelf();


//        Toast.makeText(service, "da den gio" , Toast.LENGTH_SHORT).show();
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        Toast.makeText(service, "da het gio", Toast.LENGTH_SHORT).show();
//        service.stopSelf();



    }
}
