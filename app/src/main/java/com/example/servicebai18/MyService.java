package com.example.servicebai18;

import android.app.Service;
import android.content.Intent;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class MyService extends Service {
    private MyHandler myHandler;
    private Looper looper;
    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("MyThread", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        looper = thread.getLooper();
        myHandler= new MyHandler(looper, this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = myHandler.obtainMessage();
        msg.arg1 = startId;
        myHandler.sendMessage(msg);

//        try {
//            FileOutputStream fos = openFileOutput("data.txt",getApplicationContext().MODE_APPEND);
//            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
//            for (int i=0; i<10 ; i++){
//                writer.write(String.valueOf(i));
//                writer.newLine();
//                Thread.sleep(2000);
//            }
//            writer.close();
//            fos.close();
//            stopSelf();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
       return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}